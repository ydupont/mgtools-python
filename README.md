# mgtools-python - a python port of mgtools

This project is a python port of [mgtools](https://gitlab.inria.fr/almanach/mgkit/mgtools). The aim of this project include, but are not limited to:

- go from a two-step compilation in mgtools, to a single-step compilation in mgtools-python;
- facilitate feature addition. Because of the two-step compilations, additions had to be done in two places. Now they can be done in only one;
- allow development of multiword expressions (MWEs) specific features.

# Installation

The installation is done through setuptools. If within a virtualenv:

`python setup.py install`

Otherwise, install for user only:

`python setup.py install --user`

mgtools works for both python2 and python3.

# Launch mgtools-python

After it is installed, you can launch mgtools in a terminal:

`python -m mgtools -h`

which will give you the help of mgtools, and list the different modules you can launch. To launch a specific module:

`python -m mgtools <module> <args> [options...]`

Here is the list of the modules with a brief description:

- `compare`: Compare rules from two Dyalog files. This module expects two dyalog files with their formatting removed (see `format_dyalog`).
- `format_dyalog`: Remove all formatting from a Dyalog file.
- `lexer2ply`: Write a PLY module skeleton from a yacc lexer.
- `smg2dyalog`: Compile an SMG grammar into Dyalog.
- `yacc2ply`: Write a PLY module skeleton from a yacc parser.

# Additions to mgtools

To describe MWEs, some additions were provided:

- describing trees: `tree T <: class;`;
- accessing to nodes in trees: `node(T.n)`;
- TAG operations: `substitution`, `adjuction`;
- node classes, when several nodes can be used to describe the same phenomenon: `nodeclass nd = { node1, node2 }`;
- membership guards, when a node needs to be of a certain class: `node(n1) is n2 =>`.



# Licence

mgtools-python is licenced under LPGL v3.
