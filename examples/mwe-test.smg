from .definitions import verb, noun;

class def_node_class
{
    nodeclass Subject = PreSubj | PostSubj;
    nodeclass Subject1 = {PreSubj, PostSubj} :: Arg | StrangeSubj :: Argh;
    
    PreSubj is Subject =>
        % node(T_v.PreSubj).ht.arg1.function = value(object)
        T_v/node(PreSubj).ht.arg1.function = value(object)
    ;
    
    PostSubj is Subject =>
        T_v/node(PostSubj).ht.arg2.function = value(object)
    ;
}

%===============================================================================
% verbes suivis d'un groupe nominal
% n0Vn1
%===============================================================================

class verb_noun % verb qui a un arbre noun en objet
{
    tree T_v <: verb;
    tree T_np <: noun;
    
    substitute(T_v/node(object)) = T_np; % à réécrire: T_v.object n'existe pas (argN)
}

class verb_noun_poss
{
    % MWE avec déterminant possessif en accord avec le sujet (personne, nombre)
    % :example: je fais mon apparition
    <: verb_noun;
    
    T_np/node(det).top.poss = value(+);
    T_np/node(det).top.person = T_v/node(v).person;
    T_np/node(det).top.number = T_v/node(v).number;
}

class _verb_noun_nodet
{
    % MWE avec un objet nominal non saturé
    <: verb_noun;
    T_np/node(det) = value(-);
}

class _verb_noun_det
{
    % MWE avec un objet nominal saturé
    <: verb_noun;
    T_np/node(det) = value(+);
}

class verb_noun_defdet
{
    % MWE avec un objet nominal avec un déterminant défini
    <: _verb_noun_det;
    T_np/node(det).top.def = value(+);
}

class verb_noun_indefdet
{
    % MWE avec un objet nominal avec un déterminant indéfini
    <: _verb_noun_det;
    T_np/node(det).top.def = value(-);
}

class require_de
{
    % besoin d'avoir plus générique
    % se référer à la valence du verbe => regrouper "faire l'expérience" et dire qu'on a deobj phrastique
    + require_de;
    
    clg => % clg: il [en] fait l'expérience
        T_np/node(clg).lex = value(en)
    ;
    ~ clg => % ~clg: il fait l'expérience [de] dessiner
        T_np/node(N2).lex = value(de)
    ;
}

%class verb_obj_PP1 % avec déterminant
%{
%    % :example: Verb[appeler] Obj[les choses] IObj[par leur nom]
%    % :example: Verb[avoir] Obj[des étoiles] IObj[dans les yeux]
%    
%    <: verb_noun;
%    
%    tree T_pp <: prep; % arbre prépositionnel
%    Tree T_subnp <: noun;
%    
%    node(T_v.VMod).id = value(vmod); % il existe un vmod pour le verbe 
%    
%    substitute(T_v.VMod) = T_pp;
%    substitute(T_pp.VMod.PP) = T_subnp;
%}

class verb_obj_PP2 % avec déterminant
{
    % :example: joindre le geste à la parole
    <: verb_noun;
    
    tree T_pp <: prep; % arbre prépositionnel
    Tree T_subnp <: noun;
    
    substitute(T_pp/node(N2)) = T_subnp;
    substitute(T_np/node(vmod).PP) = T_pp; % il existe un noeud réalisé en N2 pour l'objet du verbe
}

class multiverb_construct % using the PARSEME shared-task vocabulary
{
    Tree T_v1 <: verb_categorization_active;
    Tree T_v2 <: verb;

    T_v2/node(anchor).top.mode = value(infinitive);

    T_v1 < T_v2;
}


%===============================================================================
% tests exemples Parseme
%===============================================================================

class VerbNoun {
    % tree variable T_v bound to a tree inherited from class verb
    tree T_v <: verb;
    tree T_obj <: cnoun_leaf;

    T_v/desc.ht.arg1.function = value(obj);
    T_v/node(arg1::N2).id = value (object);
}

class VerbNounDef {
    %% serrer les dents
    <: VerbNoun;

    T_obj/node(det).top.def = value(+);
}

class VerbNounPossSuj {
    %% faire ses preuves
    <: VerbNoun;

    T_obj/node(det).top.poss = value(+);
    T_obj/node(det).top.numberposs = T_v/node(anchor).number;
    T_obj/node(det).top.persposs = T_v/node(anchor).person;
}

class VerbNounNoModif {
    <: VerbNoun;
    
    T_obj/node(N).adj = value(-); % no adjective on object
    T_obj/node(N2).adj = value(-); % no post-noun modifiers (PP,relatives,...
}

class NounAdjAnte {
    <: NounAdj;

    T_adj/node(anchor) < T_noun/node(anchor);
}


%===============================================================================
% some generated examples
%===============================================================================

class NounAdj {
    tree T_nc <: cnoun_leaf;
    tree T_adj_1 <: modifier_on_x;
    tree T_adj_1 <: shallow_auxiliary;

    substitute(T_nc_0/node(N2)) = T_adj_1;
}

class NounAdjAnte {
    <: NounAdj;
    
    tree T_adj_1 <: adj_before_noun;
}

class NounAdjPost {
    <: NounAdj;
    
    tree T_adj_1 <: adj_after_noun;
}
