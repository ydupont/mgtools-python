try:
    from setuptools import find_packages, setup
except ImportError:
    from distutils.core import find_packages, setup

import mgtools

setup(
    name="mgtools",
    description="Python translation of mgtools parser",
    long_description="Python translation of mgtools parser",
    license=mgtools.__license__,
    version=mgtools.__version__,
    author=mgtools.__author__,
    author_email="yoann.dupont@inria.fr",
    maintainer="Yoann Dupont",
    maintainer_email="yoann.dupont@inria.fr",
    packages=find_packages(),
    install_requires=["ply"],
    entry_points={
        'console_scripts': [
            'mgtools=mgtools.__main__:from_commandline'
        ]
    },
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5'
    ]
)
