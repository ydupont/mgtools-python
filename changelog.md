# Changelog
All notable changes to mgtools will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased
### Changed
- relative imports are now absolute imports.
### Removed
- `mgtools/pretty_print.py`: no need for a specific module for a signle function, moved `pretty_print` function to `mgtools/util.py`.

## changes compared to other versions:
