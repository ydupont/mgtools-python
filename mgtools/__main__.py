"""The main entry point of mgtools-python. The only thing it does is processing
the command line and dispatching to the right module.
"""


def from_commandline():
    from mgtools.cli import process_cl
    process_cl()


if __name__ == "__main__":
    from_commandline()
