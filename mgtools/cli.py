"""mgtools utilities for handling SMG grammars and Dyalog-formatted grammars.

Handling of commmand-line arguments for the entry point of mgtools.
This module also dispatches to the right module.
"""

import sys
import argparse

from mgtools import util

_parser = argparse.ArgumentParser(
    description=util.remove_newlines(util.group_lines(__doc__)[0])
)
_subparsers = _parser.add_subparsers()
# _subparser will be modified by each module which will define their own parser.

from mgtools import modules  # noqa F401
# importing modules is required to add subcommands to _subparsers.


def process_cl(argv=None):
    """Parse the command-line and launch the appropiate module in mgtools.

    This method is called by the entry point of mgtools. It is just a dispatcher
    to the right method with approriate parsing of the command-line before hand.
    """

    args = _parser.parse_args(args=argv)
    dargs = vars(args)
    try:
        func = dargs.pop("func")
    except KeyError:
        args = _parser.parse_args(args=["-h"])  # will exit
    func(**dargs)
    sys.exit(0)
