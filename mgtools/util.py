"""Some utility functions for mgtools package.
"""

from __future__ import print_function
from __future__ import unicode_literals

import re
import sys

__nls = re.compile("[\r\n]+")
__nls2 = re.compile("(?:\r?\n){2,}")
__spaces2 = re.compile(" {2,}")
__begspaces = re.compile("^ +")

comment = re.compile(r"^%.+\n", re.M)
indent = re.compile("^ +", re.M)
empty_line = re.compile(r"^\n", re.M)
newline = re.compile(r"([^.])\n", re.M)
other_spaces = re.compile(r" *([:,^]) *", re.M)
renew_spaces = re.compile(r":([+*-])", re.M)


def strip(item):
    """Strip comments, identation, etc. from an SMG instruction.

    Parameters
    ----------
    item : str
        the SMG instruction to strip.

    Returns
    -------
    str
        the SMG instrcution without comments and with minimal spacing.
    """
    result = comment.sub("", item)
    result = indent.sub("", result)
    result = empty_line.sub("", result)
    result = newline.sub(r"\1", result)
    result = other_spaces.sub(r"\1", result)
    result = renew_spaces.sub(r": \1", result)
    return result


def repeat_sub(pattern, replace, item, flags=0):
    """Repeat a given substitution until nothing is replaced.

    Parameters
    ----------
    pattern : str
        the re pattern to substitute
    replace : str
        the replacement string for pattern
    item : str
        the item to perform substitutions to
    flags : int, optional
        re flags, by default 0

    Returns
    -------
    str
        the modified string
    """

    ref_len = -1
    while len(item) != ref_len:
        ref_len = len(item)
        item = re.sub(pattern, replace, item, flags=re.M)
    return item


def group_lines(item):
    """This method is used to group lines separated by one or multiple
    blank lines.

    This will give a list of string, each without any newline,
    corresponding to "paragraphs".

    The main use of this method is to parse the docstring and use it for
    command-line description.

    Parameters
    ----------
    item : str
        the multiline string to group paragraph from.

    Returns
    -------
    list[str]
        the list of grouped lines
    """

    return [subitem.strip() for subitem in __nls2.split(item) if subitem.strip()]


def remove_newlines(item):
    """Remove every newline from input str.

    Parameters
    ----------
    item : str
        The input string to remove newlines from

    Returns
    -------
    str
        input string without any newline
    """
    return __nls.sub(" ", item.strip())


def pretty_print(item, output_file=sys.stdout):
    """Pretty print Dyalog represented as str.

    Parameters
    ----------
    item : str
        the Dyalog code as str
    output_file : filetype, optional
        the output file, by default sys.stdout
    """
    ref_item = strip(item)
    depths = []
    depth = 0
    i = 0
    while i < len(ref_item):
        c = ref_item[i]
        sub = ref_item[i:]
        if sub.startswith("])"):
            depth -= 1
            depths.extend([depth, depth])
            i += 2
        elif c == "]":
            depth -= 1
            depths.append(depth)
            i += 1
        elif c == "[":
            depths.append(depth)
            depth += 1
            i += 1
        elif sub.startswith("(["):
            depths.extend([depth, depth])
            depth += 1
            i += 2
        elif sub.startswith(")."):
            depth = 0
            depths.extend([depth, depth])
            i += 2
        else:
            depths.append(depth)
            i += 1
    prev_depth = depths[0]
    for i, (depth, c) in enumerate(zip(depths, ref_item)):
        if depth > prev_depth:
            print("\n" + " " * (4 * depth), end="", file=output_file)
        elif depth < prev_depth:
            print("\n" + " " * (4 * depth), end="", file=output_file)
        print(c, end="", file=output_file)
        if c == ",":
            print("\n" + " " * (4 * depth), end="", file=output_file)
        prev_depth = depth
    print(file=output_file)
