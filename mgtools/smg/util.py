#-*- coding: utf-8 -*-

"""Some utility objects and methods for SMG grammars.

This module is mainly objects that match a given production rule for generating
SMG files.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import re

from mgtools.smg.lexer import rel2name

SPACES_PER_INDENT = 4


def spaces(depth=0):
    return " " * (depth * SPACES_PER_INDENT)


noquoting = re.compile("^[a-z][0-9a-zA-Z]*$", re.M)


def quote(item):
    """Quote the item according to prolog rules so they will not
    get mistaken with variables."""

    itm = item or ""
    if isinstance(itm, facetClass):
        return itm.quote()
    else:
        itm = (itm.value if isinstance(itm, frmgString) else itm)
        if itm in "- + *" or itm.isdigit() or (itm.islower() and itm.isalnum()):
            return itm
        return "'{}'".format(itm.replace("'", "''"))


def neg_expand(item):
    if isinstance(item, AndGuardClause):
        # the "and" negation is an exclusive or of the negated elements.
        # Here, we shortcut the notations to avoid repeating terms.
        elements = item.elements
        elts = []
        for i in range(len(elements)):
            elts.append(AndGuardClause(elements[:i] + [neg_expand(elements[i])]))
        return OrGuardClause(elts)
        #return OrGuardClause([neg_expand(element) for element in item.elements])
    if isinstance(item, OrGuardClause):
        return AndGuardClause([neg_expand(element) for element in item.elements])
    if isinstance(item, EqElem):
        return EqElem(item.lhs, neg_expand(item.rhs), item.path_complete)
    elif isinstance(item, EqTerm):
        return EqTerm(neg_expand(item.root_of_path), item.path)
    elif isinstance(item, Value):
        return Value(neg_expand(item.value))
    elif isinstance(item, Disj):
        return Disj(item.values, negative=not item.negative)
    elif isinstance(item, frmgString):
        return Disj([item], negative=True)
    elif isinstance(item, str):
        return Disj([frmgString(item)], negative=True)
    elif isinstance(item, unicode):  # python2 compatibility
        return Disj([frmgString(item)], negative=True)
    else:
        raise ValueError("{} type cannot be negated".format(type(item).__name__))


class frmgString:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return quote(self.value)

    def __repr__(self):
        return repr(self.value)

    def dyalog(self, class_name, depth=0):
        return quote(self.value)


class Disable:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "disable {}".format(self.value)

    def dyalog(self, class_name, depth=0):
        return "disabled_class({}).".format(self.value)


class Grammar:
    def __init__(self, declarations=None, source=None):
        self.source = source
        self.declarations = []
        self.classes = []
        self.template_macros = {}
        self.path_macros = {}
        self.heritage = {}
        self.disabled = []
        self.imports = []

        for declaration in declarations:
            if isinstance(declaration, frmgClass):
                self.classes.append(declaration)
            elif isinstance(declaration, Disable):
                self.disabled.append(declaration)
            elif isinstance(declaration, tuple) and declaration[0] == "import":
                self.imports.append(declaration[1:])
            else:
                self.declarations.append(declaration)

    def add_class(self, declaration):
        self.classes.append(declaration)


class frmgClass:
    def __init__(self, name, doc=None, properties=None):
        self.name = name
        self.doc = doc or []
        self.properties = properties or []

    def append(self, prop):
        self.properties.append(prop)

    def extend(self, props):
        self.properties.extend(props)


class Feature:
    """A specific feature in a hypertag."""

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def dyalog(self, class_name, depth=0):
        try:
            name = self.name.dyalog(class_name)
        except Exception:
            name = quote(self.name)
        try:
            value = self.value.dyalog(class_name, depth=depth)
        except Exception:
            value = quote(self.value)

        return "{}: {}".format(name, value)


class FeatureStructure:
    """A FeatureStructure is used to describe hypertags."""

    def __init__(self, *features):
        self.features = features or []

    @classmethod
    def build(self, features):
        """Return a FeatureStructure object from (key,val) list. Val may be a
        (key,val) list, in which case, the method is called recursively."""
        def rec_build(lst, acc):
            for key, val in lst:
                if type(val) == list and isinstance(val[0], tuple):
                    acc.features.append(Feature(key, FeatureStructure.build(val)))
                else:
                    acc.features.append(Feature(key, val))
            return acc
        return rec_build(features, FeatureStructure())

    def dyalog(self, class_name, parent=None, depth=0):
        try:
            feats = self.features.dyalog(class_name, depth=depth)
        except AttributeError:  # features is a list
            if self.features:
                feats = "[{}{}{}]" \
                        .format(
                            "\n" + spaces(depth + 1),
                            (",\n" + spaces(depth + 1)).join(
                                [feat.dyalog(class_name, depth=depth + 1) for feat in self.features]
                            ),
                            "\n" + spaces(depth)
                        )
            else:
                feats = "[]"
        if isinstance(parent, Node):
            nl = ("\n" if feats != "[]" else "")
            return "nodefeature({}, {}, {}{}).".format(
                quote(class_name), parent.name.dyalog(class_name), feats, nl
            )
        else:
            return feats


class Description:
    """A description object to describe the current tree in FRMG."""

    def __init__(self, path, feat_struct):
        self.root = "description"
        self.path = None
        if path:
            if path[0] == "desc":
                self.path = path[1:]
            else:
                self.path = path[:]
            self.path = Path("", self.path)
        self.feat_struct = feat_struct

    def __str__(self):
        return "desc:{}".format(self.path)

    def dyalog(self, class_name, depth=0):
        if self.path:
            if self.path.path:
                return "desc : {}".format(self.path.dyalog(class_name, depth=depth))
            else:
                return "desc"
        else:
            return "description({}, {}).".format(
                quote(class_name), self.feat_struct.dyalog(class_name, self, depth=depth)
            )


class PathTerm:
    """A term expressed as a path."""

    def __init__(self, kind, name, path):
        self.kind = kind
        self.name = name
        self.path = path

    def __str__(self):
        if self.path:
            return "{} : {}".format(self.name, u' : '.join(self.path[0]))
        else:
            return self.name

    def dyalog(self, class_name, depth=0):
        if self.path:
            return "{}({}) : {}".format(
                self.kind, quote(self.name), u' : '.join([quote(item) for item in self.path[0]])
            )
        else:
            return "{}({})".format(self.kind, quote(self.name))


class Path:
    """A path that may be declared by macros."""

    def __init__(self, ident, path):
        self.ident = ident
        self.path = path

    def __str__(self):
        return " : ".join([str(p) for p in self.path])

    def dyalog(self, class_name, depth=0):
        if self.path:
            return " : ".join([quote(item) for item in self.path[0]])
        else:
            return ""


class Value:
    """A term representing a value."""

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)

    def dyalog(self, class_name, depth=0):
        return "value({})".format(self.value.dyalog(class_name))


class Node:
    """A Node in the Tree. It has a name and features"""

    def __init__(self, name, features, virtual=False, is_property=False):
        self.name = name
        self.features = (
            FeatureStructure.build(dict(features))
            if type(features) == list
            else features
        )
        self.virtual = virtual
        self.is_property = is_property

    def __str__(self):
        return "node {} {}".format(self.name, self.features)

    def dyalog(self, class_name, depth=0):
        if self.virtual:
            return ""

        if self.is_property:
            return "node({}, {}).".format(quote(class_name), self.name.dyalog(class_name))
        else:
            return "node({})".format(self.name.dyalog(class_name))


class Father:
    """A Father in the Tree. It has the IdName of the node it is the father of."""

    def __init__(self, idname):
        self.idname = idname

    def __str__(self):
        return "father {} {}".format(self.name, self.features)

    def dyalog(self, class_name, depth=0):
        return "father({})".format(self.idname.dyalog(class_name))


class Comment:
    def __init__(self, content):
        self.content = re.sub(r"(\r|%|\*|--)", "", content).strip()

    def __str__(self):
        return "% {}".format(self.content)


class Idname:
    def __init__(self, idents):
        self.idents = idents

    def __str__(self):
        return " : ".join([ident.dyalog("") for ident in self.idents])

    def dyalog(self, class_name, depth=0):
        return "!".join([item.dyalog(class_name) for item in self.idents])

    def extend(self, items):
        return self.idents.extend(items)


class Disj:
    """A disjunction of mulitple values."""
    def __init__(self, values, negative=False):
        self.values = values
        self.negative = negative

    def __str__(self):
        return self.dyalog("")

    def dyalog(self, class_name, depth=0):
        if len(self.values) == 1 and not self.negative:
            return self.values[0].dyalog(class_name)
        else:
            return "{}disj([{}])".format(
                "not" if self.negative else "",
                ", ".join([value.dyalog(class_name) for value in self.values])
            )


class NodeDisj:
    """A node disjunction"""

    __kind2match = {"?": "atmost", "!": "exact"}

    def __init__(self, kind, disj):
        self.match = NodeDisj.__kind2match.get(kind, kind)
        self.disj = disj

    def dyalog(self, class_name, depth=0):
        """TODO : THIS IS ONLY A FUNCTION SKELETON,
        PLEASE COMPLETE WITH ACTUAL BODY."""
        return "nodeDisj({}).".format(quote(class_name))


class frmgTree:
    def __init__(self, name, parent, is_property=False):
        self.name = name
        self.parent = parent
        self.is_property = is_property

    def __str__(self):
        return "tree {} {}".format(self.name, self.parent)

    def dyalog(self, class_name, depth=0):
        if self.is_property:
            return "tree({}, {}, {}).".format(
                quote(class_name), quote(self.name), quote(self.parent)
            )
        else:
            return "tree({})".format(quote(self.name))


class UnaryProperty:
    def __init__(self, kind, value):
        self.kind = kind
        self.value = value

    def __str__(self):
        return "{} {}".format(self.kind, self.value)

    def dyalog(self, class_name, depth=0):
        try:
            return("{}({}, {}).".format(
                self.kind, quote(class_name), self.value.dyalog(class_name, depth))
            )
        except Exception:
            return("{}({}, {}).".format(self.kind, quote(class_name), quote(self.value)))


class Relation:
    def __init__(self, kind, args):
        self.kind = rel2name.get(kind, kind).lower()
        self.args = args

    def __str__(self):
        return "{} {}".format(self.kind, u" ".join([str(arg) for arg in self.args]))

    def dyalog(self, class_name, depth=0):
        return "{}({}, {}).".format(
            self.kind,
            quote(class_name),
            ", ".join(arg.dyalog(class_name, depth) for arg in self.args)
        )


class Equation:
    def __init__(self, lhs, rhs, path_complete=None):
        self.lhs = lhs
        self.rhs = rhs
        self.path_complete = path_complete or []

    def __str__(self):
        acc = []
        if self.path_complete:
            if isinstance(self.rhs, Value):
                for path in self.path_complete:
                    acc.append(
                        "equation({0} : {2}, {1})".format(self.lhs, self.rhs, Path("", path))
                    )
            else:
                for path in self.path_complete:
                    acc.append(
                        "equation({0} : {2}, {1} : {2})".format(self.lhs, self.rhs, Path("", path))
                    )
        else:
            acc.append("equation({0}, {1})".format(self.lhs, self.rhs))

        return "\n".join(acc)

    def dyalog(self, class_name, depth=0):
        acc = []
        if self.path_complete:
            if (
                isinstance(self.rhs, Value) or (
                    isinstance(self.rhs, EqTerm) and isinstance(self.rhs.root_of_path, Value)
                )
            ):
                for path in [self.path_complete[0]] + self.path_complete[1:][::-1]:
                    acc.append("equation({0}, {1} : {3}, {2}).".format(
                        quote(class_name),
                        self.lhs.dyalog(class_name, depth),
                        self.rhs.dyalog(class_name, depth),
                        Path("", path)
                    ))
            else:
                for path in self.path_complete[0:1] + self.path_complete[1:][::-1]:
                    acc.append("equation({0}, {1} : {3}, {2} : {3}).".format(
                        quote(class_name),
                        self.lhs.dyalog(class_name, depth),
                        self.rhs.dyalog(class_name, depth),
                        Path("", path)
                    ))
        else:
            acc.append("equation({}, {}, {}).".format(
                quote(class_name),
                self.lhs.dyalog(class_name, depth),
                self.rhs.dyalog(class_name, depth)
            ))
        return "\n".join(acc)


class TagOperation:
    def __init__(self, kind, path, tree):
        self.kind = kind
        self.path = path
        self.tree = tree

    def __str__(self):
        return "{}({}, {})".format(self.kind, Path("", [self.path[0]] + self.path[1][0]), self.tree)

    def dyalog(self, class_name, depth=0):
        return "{}({}, {}, {}).".format(
            self.kind, quote(class_name), self.path.dyalog(class_name, depth), self.tree
        )


class EqTerm:
    def __init__(self, root_of_path, path=None):
        self.root_of_path = root_of_path
        self.path = path or []

    def dyalog(self, class_name, depth=0):
        path = self.path
        if path and isinstance(path[0], list):
            path = path[0]
        return " : ".join(
            [self.root_of_path.dyalog(class_name)] + [p.dyalog(class_name) for p in path]
        )


class EqElem:
    def __init__(self, lhs, rhs, path_complete=None, relation="="):
        self.lhs = lhs
        self.rhs = rhs
        self.path_complete = path_complete
        self.relation = relation

    def __str__(self):
        acc = []
        if self.path_complete:
            for path in self.path_complete:
                acc.append(
                    "EqElem({0} : {2}, {1} : {2})".format(self.lhs, self.rhs, ":".join(path))
                )
        else:
            acc.append("EqElem({0}, {1})".format(self.lhs, self.rhs))

        return "\n".join(acc)

    def dyalog(self, class_name, depth=0):
        acc = []
        if self.path_complete:
            for path in self.path_complete:
                if (
                    isinstance(self.rhs, Value) or (
                        isinstance(self.rhs, EqTerm) and isinstance(self.rhs.root_of_path, Value)
                    )
                ):
                    acc.append("{0} : {2} {3} {1}".format(
                        self.lhs.dyalog(class_name, depth),
                        self.rhs.dyalog(class_name, depth),
                        " : ".join([p.dyalog(class_name) for p in path]),
                        self.relation
                    ))
                else:
                    acc.append("{0} : {2} {3} {1} : {2}".format(
                        self.lhs.dyalog(class_name, depth),
                        self.rhs.dyalog(class_name, depth),
                        " : ".join([p.dyalog(class_name) for p in path]),
                        self.relation
                    ))
        else:
            acc.append("{0} {2} {1}".format(
                self.lhs.dyalog(class_name, depth),
                self.rhs.dyalog(class_name, depth),
                self.relation
            ))

        return (",\n" + spaces(depth)).join(acc)


class VarName:
    def __init__(self, name):
        self.name = name

    def dyalog(self, class_name, depth=0):
        return "var({})".format(self.name.dyalog(class_name))


class Join:
    def __init__(self, varname, value):
        self.varname = varname
        self.value = value

    def dyalog(self, class_name, depth=0):
        return "{} ^ {}".format(
            self.varname.dyalog(class_name, depth=depth), self.value.dyalog(class_name, depth=depth)
        )


class Guard:
    def __init__(
        self, kind, idName, eqAlt, negative=False, node_is_mandatory=False, nodeclass=None
    ):
        self.kind = kind
        self.idName = idName
        self.eqAlt = eqAlt
        self.negative = kind == "=>" and negative  # can only be negative with IMPL
        self.node_is_mandatory = node_is_mandatory
        self.nodeclass = nodeclass

    def dyalog(self, class_name, depth=0):
        if not self.nodeclass:
            part1 = "guard({0}, {1}, {2},{4}{3}\n)." \
                    .format(
                        quote(class_name),
                        self.idName.dyalog(class_name, depth),
                        self.kind,
                        self.eqAlt.dyalog(class_name, depth + 1),
                        "\n" + spaces(depth + 1)
                    )
            if self.node_is_mandatory:
                part2 = "\nguard({}, {}, -, []).".format(
                    quote(class_name),
                    self.idName.dyalog(class_name, depth)
                )
                return part1 + part2
            else:
                return part1
        else:
            return "membership({0}, {2}, {1},{4}{3}\n)." \
                .format(
                    quote(class_name),
                    self.idName.dyalog(class_name, depth),
                    self.nodeclass.dyalog(class_name, depth),
                    self.eqAlt.dyalog(class_name, depth + 1),
                    "\n" + spaces(depth + 1)
                )


class AndGuardClause:
    def __init__(self, elements):
        self.elements = []
        for elem in elements:
            if isinstance(elem, AndGuardClause):
                self.elements.extend(elem.elements)
            else:
                self.elements.append(elem)

    def dyalog(self, class_name, depth):
        return "and([{2}{0}{1}])" \
            .format(
                (",\n" + spaces(depth + 1)).join([eq.dyalog(class_name, depth + 1) for eq in self.elements]),
                "\n" + spaces(depth),
                "\n" + spaces(depth + 1)
            )


class OrGuardClause:
    def __init__(self, elements):
        self.elements = None
        if len(elements) == 1 and isinstance(elements[0], OrGuardClause):
            self.elements = elements[0].elements
        self.elements = elements

    def dyalog(self, class_name, depth):
        return "[{2}{0}{1}]" \
            .format(
                (",\n" + spaces(depth + 1)).join([eq.dyalog(class_name, depth + 1) for eq in self.elements]),
                "\n" + spaces(depth),
                "\n" + spaces(depth + 1)
            )


class NodeClass:
    def __init__(self, ident, lst):
        self.ident = ident
        self.lst = lst

    def dyalog(self, class_name, depth):
        return "nodeclass({}, {}, [{}]).".format(
            quote(class_name),
            self.ident.dyalog(class_name, depth),
            ", ".join([item.dyalog(class_name, depth) for item in self.lst])
        )


class TreeScope:
    def __init__(self, tree_name, element):
        self.tree_name = tree_name
        self.element = element

    def dyalog(self, class_name, depth):
        return "tscope({}, {})".format(
            self.element.dyalog(class_name, depth), self.tree_name
        )

class facetClass:

    def __init__ (self,class_name,facet_name,alternatives):
        self.facet_names = set([facet_name])
        self.alternatives = set(alternatives)
        if isinstance(class_name,facetClass):
            self.class_name = class_name.class_name
            self.facet_names.update(class_name.facet_names)
            self.alternatives.update(class_name.alternatives)
        else:
            self.class_name = class_name
        self.alternatives.remove(facet_name)

    def quote (self) :
        return "{} : {{{}}}".format(quote(self.class_name),u", ".join(quote(f) for f in self.facet_names))
    
class Facet:
    def __init__ (self,facet_names,content) :
        self.facet_names = facet_names
        self.content = content

    def facets (self,class_name):
        """ generate all potential facet sets """
        if isinstance(class_name,facetClass):
            for f in self.facet_names:
                if f not in class_name.alternatives:
                    yield facetClass(class_name,f,self.facet_names)
        else:
            for f in self.facet_names:
                yield facetClass(class_name,f,self.facet_names)

    def dyalog(self,class_name,depth=0) :
        return u'\n'.join((p.dyalog(facets,depth) for facets in self.facets(class_name) for p in self.content))
            
