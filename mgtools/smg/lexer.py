#-*- coding: utf-8 -*-

"""The lexer used in the yacc parser.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import ply.lex as lex


keywords = (
    'CLASS', 'DISABLE', 'FATHER', 'NODE', 'PATH', 'TEMPLATE', 'VALUE',
    # next are added keywords
    'TREE', "SUBSTITUTE", "ADJUNCT", "FROM", "IMPORT", "NODECLASS", "IS"
)

relations = ('EQUALS', 'DOMINATES', 'FATHERREL')

root_of_path = ('DESC', )

tokens = keywords + relations + root_of_path + (
    'IDENT', 'MACRONAME',
    'LCURLYBK', 'RCURLYBK', 'LSQBK', 'RSQBK', 'LPAREN', 'RPAREN',
    'INHERIT', 'MINUS', 'PLUS', 'XPLUS', 'STAR', 'NEG', 'PREC', 'FOLLOW',
    'COMMA', 'COLON', 'SEMICOLON', 'DOUBLECOLON',
    'DOT', 'ALT', 'JOIN', 'BANG', 'ATMOST', 'IMP',
    'OPT', 'DOMAIN', 'PROPERTY', 'VAR', 'XEQUAL',
    'SQSTR', 'DQSTR', "SLASH"
    # unused
    # 'ALL', 'NEWLINE', 'EQUIV', 'DOCUMENTATION', 'COMMENT', 'MLCOMMENT',
)

rel2name = {
    'FATHERREL': 'father'
}

# tokens

t_ignore = " \t"

t_ALT = r"\|"
t_ATMOST = r"\?"
t_BANG = r"\!"
t_COLON = ":"
t_COMMA = ","
t_DOT = r"\."
t_DOMINATES = r">>\+"
t_DOUBLECOLON = "::"
t_EQUALS = "="
t_FATHERREL = r">>"
t_IMP = "=>"
t_INHERIT = "<:"
t_LCURLYBK = r"\{"
t_LPAREN = r"\("
t_LSQBK = r"\["
t_MINUS = "-"
t_NEG = "~"
t_PLUS = r"\+"
t_PREC = r"<"
t_FOLLOW = r">"
t_RCURLYBK = r"\}"
t_RPAREN = r"\)"
t_RSQBK = r"\]"
t_SEMICOLON = ";"
t_STAR = r"\*"
t_VAR = r"\$"
t_JOIN = r"\^"
t_XEQUAL = r"=="
t_SLASH = r"/"

def t_NEWLINE(t):
    r'\n'
    t.lexer.lineno += 1

def t_IDENT(t):
    r'[\wàâéèêëïôüùçãíîáúûóõÀÂÉÈÊËÏÔÜÙÇÃÍÎÁÚÛÓÕ]+(-[\wàâéèêëïôüùçãíîáúûóõÀÂÉÈÊËÏÔÜÙÇÃÍÎÁÚÛÓÕ]+)*'
    # TODO: remove python2 compatibility (re module and \w)?
    if t.value.upper() in keywords + root_of_path:
        t.type = t.value.upper()
    return t

def t_MACRONAME(t):
    r'@[A-Za-z0-9_]+'
    return t

def t_SQSTR(t):
    r"'[^']+?'"
    t.value = t.value[1:-1]
    return t

def t_DQSTR(t):
    r'"[^"]+?"'
    t.value = t.value[1:-1]
    return t

def t_COMMENT(t):
    r'%+.*'
    #t.lexer.lineno += 1
    pass
    #return t

def t_MLCOMMENT(t):
    r'/\*(.|\n)+?\*/'
    t.lexer.lineno += t.value.count("\n")
    pass
    #return t

def t_error(t):
    print("Illegal character {} at line {}".format(t.value[0], t.lexer.lineno))
    t.lexer.skip(1)

# Build the lexer
lex.lex()
