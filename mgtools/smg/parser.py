#-*- coding: utf-8 -*-

"""The PLY yacc parser definition for parsing SMG files into python objets.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import os.path
import logging
import itertools

import ply.yacc as yacc

from mgtools.smg.util import (
    Grammar, frmgClass, FeatureStructure,
    Description, Path, Value, Node, Idname, NodeDisj, frmgTree,
    UnaryProperty, Relation, Equation, TagOperation, EqElem, EqTerm, Guard,
    AndGuardClause, OrGuardClause, VarName, Join, Disj, frmgString, Father, Disable,
    NodeClass, TreeScope, neg_expand, Facet
)

from mgtools.smg.lexer import (keywords, relations, root_of_path, tokens)  # noqa F401
# this import is needed to build the parser

logging.basicConfig(
    format="%(levelname)s\t%(asctime)s\t%(funcName)s\t%(message)s",
    # style="{",
    # level="WARNING",
    level = logging.WARNING,
    handlers=[logging.StreamHandler()]
)

logger = logging.getLogger("parser")

keep_comments = False

macros = {}

#==============================================================================#
#                                    LEXER                                     #
#==============================================================================#

start = 'Axiom'

#==============================================================================#
#                                    PARSER                                    #
#==============================================================================#

#===============================
# Axiom
#===============================


def p_Axiom_1(p):
    """Axiom : Dcllist"""
    logger.info("%s", [p_i for p_i in p])

    if p[1]:
        p[0] = Grammar(p[1])

#===============================
# Dcllist
#===============================


def p_Dcllist_1(p):
    """Dcllist : Dcl"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = [p[1]]


def p_Dcllist_2(p):
    """Dcllist : Dcllist Dcl"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = (p[1] or []) + [p[2]]

#===============================
# Dcl
#===============================


def p_Dcl_1(p):
    """Dcl : CLASS IDENT LCURLYBK Proplist RCURLYBK"""
    logger.info("%s", [p_i for p_i in p])

    if p[2]:
        p[0] = frmgClass(p[2], None, p[4])


def p_Dcl_2(p):
    """Dcl : TEMPLATE MACRONAME EQUALS Value"""
    logger.info("%s", [p_i for p_i in p])

    if p[2] in macros:
        raise RuntimeError("macro {} already declared".format(p[2]))

    macros[p[2]] = p[4]
    p[0] = (p[2], p[4])


def p_Dcl_3(p):
    """Dcl : PATH MACRONAME EQUALS PathList"""
    logger.info("%s", [p_i for p_i in p])

    if p[2] in macros:
        raise RuntimeError("macro {} already declared".format(p[3]))

    macros[p[2]] = p[4]
    p[0] = (p[2], p[4])


def p_Dcl_4(p):
    """Dcl : DISABLE IDENT"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Disable(frmgString(p[2]))


def p_Dcl_5(p):
    """Dcl : FROM RelativePath IMPORT IdentList SEMICOLON"""
    logger.info("%s", [p_i for p_i in p])
    p[0] = ("import", p[2], p[4])


def p_RelativePath_1(p):
    """RelativePath : Dots IDENT"""
    p[0] = os.path.join(*tuple(p[1] + [p[2] + ".smg"]))


def p_Dots_1(p):
    """Dots : DOT"""
    p[0] = ["."]


def p_Dots_2(p):
    """Dots : DOT Dots"""
    p[0] = [".."] + (p[2] or [])


def p_IdentList_1(p):
    """IdentList : IDENT"""
    p[0] = [p[1]]


def p_IdentList_2(p):
    """IdentList : IdentList COMMA IDENT"""
    p[0] = (p[1] or []) + [p[3]]

#===============================
# Proplist
#===============================


def p_Proplist_1(p):
    """Proplist : """
    logger.info("%s", [p_i for p_i in p])



def p_Proplist_2(p):
    """Proplist : XProperty Proplist"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = [p[1]]
    if p[2]:
        p[0].extend(p[2])

#===============================
# Property
#===============================

def p_XProperty_2 (p):
    """XProperty : Property SEMICOLON"""

    p[0] = p[1]

def p_XProperty_1 (p):
    """XProperty : Facet"""

    p[0] = p[1]
    
def p_Property_1(p):
    """Property : Heritage
                | Pere
                | Ancetre
                | Precedence
                | Equality
                | Desc
                | Equation
                | Node
                | Needs
                | Provides
                | Guard
                | Atomic
                | NodeDisj
                | Tree
                | TagOp
                | Nodeclass"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]
    p[0].is_property = True

#===============================
# Heritage
#===============================


def p_Heritage_1(p):
    """Heritage : INHERIT IDENT"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = UnaryProperty("super", frmgString(p[2]))

#===============================
# Pere
#===============================


def p_Pere_1(p):
    """Pere : Idname FATHERREL Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Relation("FATHERREL", (p[1], p[3]))

#===============================
# Ancetre
#===============================


def p_Ancetre_1(p):
    """Ancetre : Idname DOMINATES Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Relation("DOMINATES", (p[1], p[3]))

#===============================
# Precedence
#===============================


def p_Precedence_1(p):
    """Precedence : Idname PREC Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Relation("PRECEDES", (p[1], p[3]))


def p_Precedence_2(p):
    """Precedence : EqTerm PREC EqTerm"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Relation("PRECEDES", (p[1], p[3]))


#===============================
# Equality
#===============================


def p_Equality_1(p):
    """Equality : Idname EQUALS Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Relation("EQUALS", (p[1], p[3]))

#===============================
# Desc
#===============================


def p_Desc_1(p):
    """Desc : DESC LPAREN LSQBK Features RSQBK RPAREN"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Description(None, FeatureStructure.build(p[4]))

#===============================
# Features
#===============================


def p_Features_1(p):
    """Features : """
    logger.info("%s", [p_i for p_i in p])

    p[0] = []


def p_Features_2(p):
    """Features : F"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = [p[1]]


def p_Features_3(p):
    """Features : Features COMMA F"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = (p[1] or []) + [p[3]]

#===============================
# F
#===============================


def p_F_1(p):
    """F : IDENT COLON Value"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = (p[1], p[3])

#===============================
# Guard
#===============================


def p_Guard_1(p):
    """Guard : Idname IMP OrGuardClause"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Guard(frmgString("+"), p[1], p[3])


def p_Guard_2(p):
    """Guard : NEG Idname IMP OrGuardClause"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Guard(frmgString("-"), p[2], p[4], negative=True)


def p_Guard_3(p):
    """Guard : Idname PLUS OrGuardClause"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Guard(frmgString("+"), p[1], p[3], node_is_mandatory=True)


def p_Guard_4(p):
    """Guard : Idname XPLUS OrGuardClause"""
    logger.info("%s", [p_i for p_i in p])

    raise NotImplementedError("Unhandled rule => {}".format(p_Guard_4.__doc__))
    p[0] = p[1]


def p_Guard_5(p):
    """Guard : Idname IS IDENT IMP OrGuardClause"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Guard(frmgString("+"), p[1], p[5], nodeclass=frmgString(p[3]), node_is_mandatory=False)

#===============================
# OrGuardClause
#===============================


def p_OrGuardClause_1(p):
    '''OrGuardClause : AndGuardClause'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = OrGuardClause([p[1]])


def p_OrGuardClause_2(p):
    '''OrGuardClause : ImpGuardClause'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_OrGuardClause_3(p):
    '''OrGuardClause : AndGuardClause ALT OrGuardClause'''
    logger.info("%s", [p_i for p_i in p])

    if p[3]:
        p[0] = OrGuardClause([p[1]] + p[3].elements)

#===============================
# AndGuardClause
#===============================


def p_AndGuardClause_1(p):
    '''AndGuardClause : GuardElem'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = AndGuardClause([p[1]])


def p_AndGuardClause_2(p):
    '''AndGuardClause : GuardElem COMMA AndGuardClause'''
    logger.info("%s", [p_i for p_i in p])

    if p[3]:
        p[0] = AndGuardClause([p[1]] + p[3].elements)

#===============================
# ImpGuardClause
#===============================


def p_ImpGuardClause_1(p):
    '''ImpGuardClause : AndGuardClause IMP AndGuardClause'''
    logger.info("%s", [p_i for p_i in p])

    if p[1] and p[3]:
        p[0] = OrGuardClause([
            AndGuardClause(p[1].elements + p[3].elements),
            AndGuardClause(
                [neg_expand(p[1])]
            )
        ])


def p_ImpGuardClause_2(p):
    '''ImpGuardClause : AndGuardClause IMP AndGuardClause ALT AndGuardClause'''
    logger.info("%s", [p_i for p_i in p])

    if p[1] and p[3]:
        p[0] = OrGuardClause([
            AndGuardClause([p[1]] + [p[3]]),
            AndGuardClause(
                [neg_expand(p[1])] + [p[5]]
            )
        ])

#===============================
# GuardElem
#===============================


def p_GuardElem_1(p):
    '''GuardElem : EqElem'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_GuardElem_2(p):
    '''GuardElem : LPAREN OrGuardClause RPAREN'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[2]


def p_GuardElem_3(p):
    '''GuardElem : NEG LPAREN OrGuardClause RPAREN'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = neg_expand(p[3])

#===============================
# EqElem
#===============================


def p_EqElem_1(p):
    '''EqElem : EqTerm EQUALS EqTerm'''
    logger.info("%s", [p_i for p_i in p])

    if p[1] and p[3]:
        p[0] = EqElem(p[1], p[3], None)


def p_EqElem_2(p):
    """EqElem : EqTerm XEQUAL EqTerm"""
    logger.info("%s", [p_i for p_i in p])

    if p[1] and p[3]:
        p[0] = EqElem(p[1], p[3], None, relation=p[2])


def p_EqElem_3(p):
    '''EqElem : EqElem PathComplete'''
    logger.info("%s", [p_i for p_i in p])

    if p[1] and p[2]:
        p[0] = EqElem(p[1].lhs, p[1].rhs, p[2])

#===============================
# Atomic
#===============================


def p_Atomic_1(p):
    """Atomic : BANG EqTerm"""
    logger.info("%s", [p_i for p_i in p])

    raise NotImplementedError("Unhandled rule => {}".format(p_Atomic_1.__doc__))

#===============================
# Equation
#===============================


def p_Equation_1(p):
    """Equation : EqTerm EQUALS EqTerm PathComplete"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Equation(p[1], p[3], p[4])


def p_Equation_2(p):
    """Equation : EqTerm EQUALS EqTerm"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Equation(p[1], p[3])

#===============================
# PathListElem
#===============================


def p_PathListElem_1(p):
    """PathListElem : LCURLYBK PathList RCURLYBK"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[2]

#===============================
# PathComplete
#===============================


def p_PathComplete_1(p):
    """PathComplete : PathListElem"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_PathComplete_2(p):
    """PathComplete : PathComplete PathListElem"""
    logger.info("%s", [p_i for p_i in p])

    if p[1]:
        lst = [left[:] + right[:] for left, right in itertools.product(p[1], p[2])]
    else:
        lst = p[2][:]
    p[0] = lst

#===============================
# XPath
#===============================


def p_XPath_1(p):
    """XPath : Path"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_XPath_2(p):
    """XPath : NEG Path"""
    logger.info("%s", [p_i for p_i in p])

    raise NotImplementedError("Unhandled rule => {}".format(p_XPath_2.__doc__))

#===============================
# PathList
#===============================


def p_PathList_1(p):
    """PathList : XPath"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_PathList_2(p):
    """PathList : PathList COMMA XPath"""
    logger.info("%s", [p_i for p_i in p])

    if p[3]:
        p[0] = p[3]
        if p[1][0] != "~":
            for item in p[1][::-1]:
                if item not in p[0]:
                    p[0].insert(0, item)
        else:
            for item in p[1][1]:
                if item in p[0]:
                    p[0].remove(item)

#===============================
# NodeDisj
#===============================


def p_NodeDisj_1(p):
    """NodeDisj : BANG NodeDisjAux
                | ATMOST NodeDisjAux"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = NodeDisj(p[1], p[2])

#===============================
# NodeDisjAux
#===============================


def p_NodeDisjAux_1(p):
    """NodeDisjAux : NodeName"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]

#===============================
# NodeName
#===============================


def p_NodeName_1(p):
    """NodeName : Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]

#===============================
# EqTerm
#===============================


def p_EqTerm_1(p):
    """EqTerm : PreRoot RootOfPath Path"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = TreeScope(p[1], EqTerm(p[2], p[3] or []))


def p_EqTerm_2(p):
    """EqTerm : RootOfPath Path"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = EqTerm(p[1], p[2] or [])


def p_EqTerm_3(p):
    """EqTerm : VALUE LPAREN Value RPAREN"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = EqTerm(Value(p[3]))


def p_EqTerm_4(p):
    """EqTerm : DOMAIN LPAREN IDENT RPAREN"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = EqTerm(Domain(p[3]))


def p_EqTerm_5(p):
    """EqTerm : PROPERTY LPAREN IDENT RPAREN"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = EqTerm(Property(p[3]))


#===============================
# PreRoot
#===============================


def p_PreRoot_1(p):
    """PreRoot : IDENT SLASH"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = frmgString(p[1])

#===============================
# Varname
#===============================


def p_Varname_1(p):
    """Varname : VAR Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = VarName(p[2])

#===============================
# AtomAlts
#===============================


def p_AtomAlts_1(p):
    """AtomAlts : Atom"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Disj([p[1]])


def p_AtomAlts_2(p):
    """AtomAlts : Atom ALT AtomAlts"""
    logger.info("%s", [p_i for p_i in p])

    if p[3]:
        p[0] = Disj([p[1]] + p[3].values)

#===============================
# Atom
#===============================


def p_Atom_1(p):
    """Atom : IDENT
            | STAR
            | PLUS
            | MINUS
            | SQSTR
            | DQSTR"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = frmgString(p[1])

#===============================
# NValue
#===============================


def p_NValue_1(p):
    """NValue : Atom"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Disj([p[1]])


def p_NValue_2(p):
    """NValue : Atom ALT AtomAlts"""
    logger.info("%s", [p_i for p_i in p])

    if p[3]:
        p[0] = Disj([p[1]] + p[3].values)

#===============================
# Value
#===============================


def p_Value_1(p):
    """Value : NValue"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_Value_2(p):
    """Value : NEG NValue"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = neg_expand(p[2])


def p_Value_3(p):
    """Value : Varname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_Value_4(p):
    """Value : Varname JOIN Value"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Join(p[1], p[3])


def p_Value_5(p):
    """Value : MACRONAME"""
    logger.info("%s", [p_i for p_i in p])

    value = macros[p[1]]

    if isinstance(value, list):  # path
        raise ValueError("{} is a path not a value".format(p[1]))

    p[0] = value


def p_Value_6(p):
    """Value : LSQBK Features RSQBK"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = FeatureStructure.build(p[2])


def p_Value_7(p):
    """Value : LPAREN IDENT RPAREN LSQBK Features RSQBK"""
    logger.info("%s", [p_i for p_i in p])

    raise NotImplementedError("Unhandled rule => {}".format(p_Value_7.__doc__))

#===============================
# Node
#===============================


def p_Node_1(p):
    """Node : VNode"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]


def p_Node_2(p):
    """Node : OPT VNode"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Optional(p[1])

#===============================
# VNode
#===============================


def p_VNode_1(p):
    """VNode : RawNode"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Node(virtual=True, **p[1])


def p_VNode_2(p):
    """VNode : NODE RawNode"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Node(virtual=False, **p[2])

#===============================
# RawNode
#===============================


def p_RawNode_1(p):
    """RawNode : Idname COLON Value"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = {"name": p[1], "features": p[3]}

#===============================
# Path
#===============================


def p_Path_1(p):
    """Path : """
    logger.info("%s", [p_i for p_i in p])


def p_Path_2(p):
    """Path : DOT IDENT Path"""
    logger.info("%s", [p_i for p_i in p])

    if p[3]:
        path = [[frmgString(p[2])] + item for item in p[3]]
        p[0] = path
    else:
        p[0] = [[frmgString(p[2])]]


def p_Path_3(p):
    """Path : DOT MACRONAME Path"""
    logger.info("%s", [p_i for p_i in p])

    path = macros[p[2]]

    if not isinstance(path, list):
        raise ValueError("{} is not a path".format(p[2]))

    if p[3]:
        p[0] = [left[:] + right[:] for left, right in itertools.product(path, p[3])]
    else:
        p[0] = path[:]

#===============================
# RootOfPath
#===============================


def p_RootOfPath_1(p):
    """RootOfPath : DESC"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = frmgString(p[1])


def p_RootOfPath_2(p):
    """RootOfPath : FATHER LPAREN Idname RPAREN"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Father(p[3])


def p_RootOfPath_3(p):
    """RootOfPath : NODE LPAREN Idname RPAREN"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Node(p[3], [])


def p_RootOfPath_4(p):
    """RootOfPath : Varname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]

#===============================
# Needs
#===============================


def p_Needs_1(p):
    """Needs : MINUS Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = UnaryProperty("needs", p[2])

#===============================
# Provides
#===============================


def p_Provides_1(p):
    """Provides : PLUS Idname"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = UnaryProperty("provides", p[2])

#===============================
# Idname
#===============================


def p_Idname_1(p):
    """Idname : IDENT"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = Idname([frmgString(p[1])])


def p_Idname_2(p):
    """Idname : Idname DOUBLECOLON IDENT"""
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]
    if p[3]:
        p[0].extend([frmgString(p[3])])

#===============================
# Tree
#===============================


def p_Tree_1(p):
    '''Tree : TREE IDENT INHERIT IDENT'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = frmgTree(p[2], p[4])

#===============================
# TagOp
#===============================


def p_TagOp_1(p):
    '''TagOp : TreeOp LPAREN TreeTerm RPAREN EQUALS TreeIdent'''
    logger.info("%s", [p_i for p_i in p])

    if p[1] and p[3] and p[6]:
        p[0] = TagOperation(p[1], p[3], p[6])

#===============================
# TreeOp
#===============================


def p_TreeOp_1(p):
    '''TreeOp : SUBSTITUTE
              | ADJUNCT'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]

#===============================
# TreeIdent
#===============================


def p_TreeIdent_1(p):
    '''TreeIdent : IDENT
                 | STAR'''
    logger.info("%s", [p_i for p_i in p])

    p[0] = p[1]

#===============================
# TreeTerm
#===============================


def p_TreeTerm_1(p):
    '''TreeTerm : IDENT SLASH RootOfPath Path'''

    try:
        p3_str = ' : '.join(item.value for item in p[3].name.idents)
    except AttributeError:
        p3_str = p[3].value
    lst = [p3_str]
    if p[4]:
        lst.extend(p[4][0])
    p[0] = TreeScope(frmgString(p[1]), EqTerm(p[3], p[4] or []))

#===============================
# nodeclass
#===============================


def p_nodeclass_1(p):
    '''Nodeclass : NODECLASS IDENT EQUALS NodeClassAlts'''

    p[0] = NodeClass(frmgString(p[2]), p[4])


def p_identalts_1(p):
    '''NodeClassAlts : NodeClassAlt'''
    p[0] = p[1]


def p_identalts_2(p):
    '''NodeClassAlts : NodeClassAlt ALT NodeClassAlts'''

    if p[3]:
        p[0] = p[1] + p[3]


def p_NodeClassAlt_1(p):
    """NodeClassAlt : IDENT"""
    p[0] = [frmgString(p[1])]


def p_NodeClassAlt_2(p):
    """NodeClassAlt : IDENT DOUBLECOLON IDENT"""
    p[0] = [Path("", [[frmgString(p[1]), frmgString(p[3])]])]


def p_NodeClassAlt_3(p):
    """NodeClassAlt : LCURLYBK IdentList RCURLYBK DOUBLECOLON IDENT"""
    p[0] = [Path("", [[item, frmgString(p[5])]]) for item in p[2]]

#===============================
# Facet
#===============================

def p_Facet(p):
    '''Facet : PREC IdentList FOLLOW LCURLYBK Proplist RCURLYBK'''

    p[0] = Facet(p[2],p[5])
    
##===============================
## Doc
##===============================
#
#def p_Doc_1(p):
#    """Doc : """
#    logger.info("%s", [p_i for p_i in p])
#
#def p_Doc_2(p):
#    """Doc : Documentation Doc"""
#    logger.info("%s", [p_i for p_i in p])
#    p[0] = p[1] + (p[2] or [])
#
#def p_Documentation_1(p):
#    '''Documentation : COMMENT'''
#    logger.info("%s", [p_i for p_i in p])
#
#    comment_str = p[1]
#    if "--" not in comment_str:
#        p[0] = [Comment(comment_str)]
#
#def p_Doc1_2(p):
#    '''Documentation : MLCOMMENT'''
#    logger.info("%s", [p_i for p_i in p])
#
#    comment_str = p[1][2:-2]
#    p[0] = []
#    for cmt in comment_str.replace("\r","").split("\n"):
#        if "--" in cmt: continue
#        if cmt:
#            p[0].append(Comment(cmt))


def p_error(p):
    raise SyntaxError(
        'yacc: Syntax error at line {}, token={} value="{}"'.format(p.lineno, p.type, p.value)
    )


smg_parser = yacc.yacc(debug=False, write_tables=False)
