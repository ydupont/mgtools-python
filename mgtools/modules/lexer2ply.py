"""Write a PLY module skeleton from a yacc lexer.
"""

from __future__ import print_function
from __future__ import unicode_literals
from io import open

import os.path
import re
from mgtools import util
from mgtools.cli import _subparsers


def minimal_escape(item):
    """Very similar to re.escape, but will not espace some characters that
    should not be escaped in dyalog.
    """

    return re.sub(r"([()[\]{}^$|?!*.+-])", r"\\\1", item)


def main(inputfile, outputfile, encoding="utf-8", force=False):
    mode = ("w" if force else "x")

    with open(inputfile, "r", encoding=encoding, newline="") as ifd:
        content = ifd.read()

    # cleaning to keep only the rules of the parser
    parts = re.findall(
        r'^["\w\'"@\\({][^\r\n]+ \{.+?return[^{}]+?\}\s*;?\n',
        content,
        flags=re.I + re.M + re.DOTALL
    )
    name2tok = {}
    for i, part in enumerate(parts):
        try:
            token, name = re.findall("^([^ ]+).+ return ([^ ]+)", part, re.DOTALL)[0]
            token = token.strip('"')
            name = name.strip()
            if ";" not in name:
                name2tok[name] = token
        except IndexError:
            pass

    # outputting PLY lexer code to outputfile
    with open(outputfile, mode, encoding=encoding, newline="") as ofd:
        ofd.write("import ply.lex as lex\n")
        ofd.write("\n")
        tokens = "tokens = " + repr(tuple(sorted(name2tok.keys())))
        indices = [match.end() for match in re.finditer(",", tokens)]
        lo = 0
        hi = -1
        shift = 0
        while lo < len(tokens):
            for i, index in enumerate(indices):
                if index - lo + shift > 80:
                    break
                else:
                    hi = indices[i]
            if index - lo + shift < 80:
                hi = len(tokens)
            ofd.write(" " * shift + tokens[lo: hi].strip() + "\n")
            lo = hi
            shift = 4
        ofd.write("\n")
        for name, token in sorted(name2tok.items()):
            ofd.write("t_{} = r'{}'\n".format(name, minimal_escape(token)))


parser = _subparsers.add_parser(
    os.path.splitext(os.path.basename(__file__))[0],
    description=util.remove_newlines(util.group_lines(__doc__)[0])
)
parser.add_argument("inputfile",
                    help="The input lexer file")
parser.add_argument("outputfile",
                    help="The output ply file")
parser.add_argument("-e", "--encoding", default="utf-8",
                    help="The encoding")
parser.add_argument("-f", "--force", action="store_true",
                    help="The force writing (default: false)")
parser.set_defaults(func=main)
