"""Write a PLY module skeleton from a yacc parser.
"""

from __future__ import print_function
from __future__ import unicode_literals
from io import open

import os.path
import re

from mgtools import util
from mgtools.cli import _subparsers


def main(inputfile, outputfile, encoding="utf-8", force=False):
    mode = ("w" if force else "x")

    with open(inputfile, "r", encoding=encoding, newline="") as ifd:
        content = ifd.read()

    # cleaning to keep only the rules of the parser
    parts = re.findall("^\\w+ *:.+?;\n\n", content, flags=re.I + re.M + re.DOTALL)
    for i, part in enumerate(parts):
        part = part.strip()[:-1]
        part = util.repeat_sub("\\{[^{}]+\\}", "", part, flags=re.M)
        part = util.repeat_sub("\n[ \t]+", " ", part, flags=re.M)
        part = re.sub(" {2,}", " ", part)
        parts[i] = part

    # outputting PLY skeleton code to outputfile
    with open(outputfile, mode, encoding=encoding, newline="") as ofd:
        print("import logging", file=ofd)
        print(file=ofd)
        print(
            r'logging.basicConfig(format="{levelname}\t{asctime}\t{funcName}\t{message}",'
            r' style="{", level="WARNING", handlers=[logging.StreamHandler()])',
            file=ofd
        )
        print('logger = logging.getLogger("parser")', file=ofd)
        print(file=ofd)
        for part in parts:
            rule_name = part[: part.index(":")].strip()
            alts_str = part[part.index(":") + 1:]
            alts = [alt_str.strip() for alt_str in alts_str.split("|")]
            print("#" + "=" * 31, file=ofd)
            print("# {}".format(rule_name), file=ofd)
            print("#" + "=" * 31, file=ofd)
            print(file=ofd)
            if all(len(alt.split()) == 1 for alt in alts):
                sep = "\n" + " " * (4 + 3 + len(rule_name) + 1) + "| "
                function_name = "p_{}_1".format(rule_name)
                print("def {}(p):".format(function_name), file=ofd)
                print('    """{} : {}"""'.format(rule_name, sep.join(alts)), file=ofd)
                print('    logger.info("{} at line %s", p.lineno)'.format(rule_name), file=ofd)
                print(file=ofd)
                print(
                    '    raise NotImplementedError("Unhandled rule =>'
                    ' {{}}".format({}.__doc__))'.format(function_name),
                    file=ofd
                )
                print(file=ofd)
            else:
                for i, alt in enumerate(alts, 1):
                    function_name = "p_{}_{}".format(rule_name, i)
                    print("def {}:".format(function_name), file=ofd)
                    print('    """{} : {}"""'.format(rule_name, alt), file=ofd)
                    print('    logger.info("{} at line %s", p.lineno)'.format(rule_name), file=ofd)
                    print(file=ofd)
                    if alt:
                        print(
                            '    raise NotImplementedError("Unhandled rule =>'
                            ' {{}}".format({}.__doc__))'.format(function_name),
                            file=ofd
                        )
                        print(file=ofd)


parser = _subparsers.add_parser(
    os.path.splitext(os.path.basename(__file__))[0],
    description=util.remove_newlines(util.group_lines(__doc__)[0])
)
parser.add_argument("inputfile",
                    help="The input yacc file")
parser.add_argument("outputfile",
                    help="The output ply file")
parser.add_argument("-e", "--encoding", default="utf-8",
                    help="The encoding")
parser.add_argument("-f", "--force", action="store_true",
                    help="The force writing (default: false)")
parser.set_defaults(func=main)
