"""Remove all formatting from a Dyalog file.
"""

from __future__ import unicode_literals
from io import open

import os.path
from mgtools import util
from mgtools.cli import _subparsers


def main(infilename, outfilename, encoding="utf-8", force=False):
    outmode = ("w" if force else "x")

    with open(infilename, "r", encoding=encoding, newline="") as ifd:
        content = ifd.read()
    content = util.strip(content)

    try:
        with open(outfilename, outmode, encoding=encoding, newline="") as ofd:
            ofd.write(content)
    except ValueError:  # "x" mode not supported in python2: mimicing python3 behaviour
        if os.path.exists(outfilename):
            import errno
            raise IOError(errno.EEXIST, "File exists: '{}'".format(outfilename))
        with open(outfilename, "w", encoding=encoding, newline="") as ofd:
            ofd.write(content)


parser = _subparsers.add_parser(
    os.path.splitext(os.path.basename(__file__))[0],
    description=util.remove_newlines(util.group_lines(__doc__)[0])
)
parser.add_argument("infilename",
                    help="The input SMG file")
parser.add_argument("outfilename",
                    help="The output SMG file")
parser.add_argument("-e", "--encoding", default="utf-8",
                    help="The encoding")
parser.add_argument("-f", "--force", action="store_true",
                    help="Force overwritting of the output file")
parser.set_defaults(func=main)
