# coding: utf-8

"""Compile an SMG grammar into Dyalog.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
from io import open, TextIOWrapper

import os.path
import sys

import mgtools.smg.parser
from mgtools.smg.parser import smg_parser
from mgtools.smg.util import (quote, Comment, Node)
from mgtools import util
from mgtools.cli import _subparsers


def parse(inputfile, encoding="utf-8", *args, **kwargs):
    """Parse the grammar described in SMG file given in argument.

    Parameters
    ----------
    inputfile : pathlike or file
        the input file where to read the grammar.
    encoding : str, optional
        the encoding of input file, by default "utf-8"

    Returns
    -------
    mgtools.smg.util.Grammar
        the parsed metagrammar
    """

    try:
        with open(inputfile, "r", encoding=encoding) as fd:
            content = fd.read()
    except TypeError:  # inputfile is a file
        wrap = TextIOWrapper(sys.stdin.buffer, encoding=encoding)
        content = wrap.read()
    mg = smg_parser.parse(content)
    mg.source = inputfile
    return mg


def output(mg, keep_comments=False, encoding="utf-8"):
    """Output a metagrammar to standard output using Dyalog syntax.

    Parameters
    ----------
    mg : mgtools.smg.util.Grammar
        the metagrammar to output
    keep_comments : bool, optional
        keep the comments in code, by default False
    encoding : str, optional
        the output encoding, by default "utf-8"
    """

    for disabled in mg.disabled:
        print(disabled.dyalog(""))
    print()

    for filename, classes in mg.imports:
        try:
            folder = os.path.dirname(mg.source)
        except AttributeError:
            folder = "."
        mg_ = smg_parser.parse(open(os.path.join(folder, filename), "r").read())
        for cls in [cls for cls in mg_.classes if cls.name in classes]:
            print("%" + "=" * 79)
            print("% {}".format(cls.name))
            print("%" + "=" * 79)
            print()
            print("class({}).".format(quote(cls.name)))
            if keep_comments:
                for doc in cls.doc:
                    print(doc)
            for prop in cls.properties:
                if isinstance(prop, Comment) and not keep_comments:
                    continue

                prop_str = prop.dyalog(cls.name, 0)
                if prop_str:
                    print(prop_str)
                if isinstance(prop, Node):
                    print(prop.features.dyalog(cls.name, prop, depth=0))
            print()

    for cls in mg.classes:
        print("%" + "=" * 32)
        print("% {}".format(cls.name))
        print("%" + "=" * 32)
        print()
        print("class({}).".format(quote(cls.name)))
        if keep_comments:
            for doc in cls.doc:
                print(doc)
        for prop in cls.properties:
            if isinstance(prop, Comment) and not keep_comments:
                continue

            prop_str = prop.dyalog(cls.name, 0)
            if prop_str:
                try:
                    print(prop_str)
                except UnicodeEncodeError:
                    print(prop_str.encode(encoding))
            if isinstance(prop, Node):
                data = prop.features.dyalog(cls.name, prop, depth=0)
                try:
                    print(data)
                except UnicodeEncodeError:
                    print(data.encode(encoding))
        print()


def main(inputfile, encoding="utf-8", debug=False, *args, **kwargs):
    if debug:
        mgtools.smg.parser.logger.setLevel("DEBUG")
    output(parse(inputfile, encoding=encoding, *args, **kwargs), encoding=encoding)


parser = _subparsers.add_parser(
    os.path.splitext(os.path.basename(__file__))[0],
    description=util.remove_newlines(util.group_lines(__doc__)[0])
)
parser.add_argument(
    "inputfile",
    nargs="?",
    default=None,
    help="The input SMG file"
)
parser.add_argument("-e", "--encoding", default="utf-8",
                    help="The encoding")
parser.add_argument("-d", "--debug", action="store_true",
                    help="Activate debug output")
parser.set_defaults(func=main)
