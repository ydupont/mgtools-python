#-*- coding: utf-8 -*-

"""Compare rules from two Dyalog files.
This module expects two dyalog files with their formatting removed.
"""

from __future__ import absolute_import
from __future__ import unicode_literals
from io import open

import os.path
import re

from mgtools import util
from mgtools.cli import _subparsers

whitespaces = re.compile(r"\s+")


def main(reference, comparison, encoding="utf-8"):
    ref_lines = set()
    cmp_lines = set()
    diffs = []
    with open(reference, "r", encoding=encoding, newline="") as rfd, \
            open(comparison, "r", encoding=encoding, newline="") as cfd:
        try:
            while True:
                rline = whitespaces.sub("", next(rfd))
                cline = whitespaces.sub("", next(cfd))
                ref_lines.add(rline)
                cmp_lines.add(cline)

                rline = rline \
                    .replace("Ã§", "ç") \
                    .replace("Ãª", "ê") \
                    .replace("Ã¨", "è") \
                    .replace("Ã¹", "ù") \
                    .replace("Ã´", "ô") \
                    .replace("Ã", "à")

                if rline != cline:
                    diffs.append((rline, cline))
        except StopIteration:
            pass
    diffs = [diff for diff in diffs if not (diff[0] in cmp_lines and diff[1] in ref_lines)]

    with open("ref.txt", "w", encoding="utf-8", newline="") as ref_fd, \
            open("cmp.txt", "w", encoding="utf-8", newline="") as cmp_fd:
        for ref_line, cmp_line in diffs:
            util.pretty_print(ref_line + "\n", ref_fd)
            util.pretty_print(cmp_line + "\n", cmp_fd)


parser = _subparsers.add_parser(
    os.path.splitext(os.path.basename(__file__))[0],
    description=util.remove_newlines(util.group_lines(__doc__)[0])
)
parser.add_argument("reference",
                    help="The input SMG file")
parser.add_argument("comparison",
                    help="The output SMG file")
parser.add_argument("-e", "--encoding", default="utf-8",
                    help="The encoding")
parser.set_defaults(func=main)
